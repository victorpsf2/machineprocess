class ProcessMachine {
  constructor() {
    this.os = require("os");
    this.INTERVAL = 1000;
  }

  async cpuFree() {
    return await this.getCpuUsage(true);
  }

  async cpuUsage() {
    return await this.getCpuUsage(false);
  }

  getCpuUsage(free) {
    return new Promise(async (resolve) => {
      var stats1 = await this.cpuInfo();
      var startIdle = stats1.idle;
      var startTotal = stats1.total;


      setTimeout(async () => {
        var stats2 = await this.cpuInfo();
        var endIdle = stats2.idle;
        var endTotal = stats2.total;

        var idle = endIdle - startIdle;
        var total = endTotal - startTotal;
        var perc = idle / total;

        if (free) {
          resolve(perc);
        }
        resolve(perc - 1);

      }, this.INTERVAL);
    });
  }

  async cpuInfo() {
    var cpus = this.os.cpus();
    var process = {};

    cpus.forEach((cpu) => {
      Object.keys(cpu.times).forEach((key) => {
        if (process[key] === undefined) {
          process[key] = cpu.times[key];
        } else {
          process[key] += cpu.times[key];
        }
      });
    });

    var total = 0;
    Object.keys(process).forEach((key) => {
      total += process[key];
    });
    return {
      "idle": process.idle,
      "total": total
    };
  }
}

module.exports = new ProcessMachine();

