class ReadProcess
{
  constructor() {
    this.Memory = require("./memory");
    this.Cpu    = require("./cpu");
  }

  sleep(time)
  {
    return new Promise((resolve) =>
      {
        setTimeout(() =>
        {
           resolve(true);
        }, time);
      });
   }


  async _rayp_()
  {
    setInterval(async () =>
    {
       console.log(
         await this.Memory.memoryUsage()
       );
       console.log(
         await this.Cpu.cpuFree()
       );
    }, 2000);
  }
}

(new ReadProcess)._rayp_();
