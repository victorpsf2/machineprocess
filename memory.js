class MemoryMachine
{
  constructor ()
  {
    this.os = require("os");
  }

  async memoryUsage()
  {
    let memoryinfo = {};

    memoryinfo.totalmem = (
      (this.os.totalmem()/1024)
      /1024
    );

    memoryinfo.freemem = (
      (this.os.freemem()/1024)
      /1024
    );

    memoryinfo.usagemem = (
      memoryinfo.totalmem -
      memoryinfo.freemem
    );

    return memoryinfo;
  }
}

module.exports = new MemoryMachine();
